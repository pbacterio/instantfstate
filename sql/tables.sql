--CREATE DATABASE IF NOT EXISTS instantfstate;
--CREATE USER IF NOT EXISTS instantfstate WITH PASSWORD 'instantfstate';
--GRANT ALL ON DATABASE instantfstate TO instantfstate;
CREATE TABLE locks (
    stateid VARCHAR(44) NOT NULL,
    content BYTEA,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE INDEX ON locks (stateid);

CREATE TABLE states (
    stateid VARCHAR(44) NOT NULL,
    content BYTEA,
    created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
CREATE INDEX ON states (stateid);
