.EXPORT_ALL_VARIABLES:

PYTHONDONTWRITEBYTECODE = true

ifeq ($(USE_DOCKER),)
  CONTAINER = podman
else
  CONTAINER = docker
endif

CI_REGISTRY_IMAGE ?= registry.gitlab.com/pbacterio/instantfstate

test:
	python -m pytest -v
	mypy instantfstate

run:
	python3 -m instantfstate

container-build:
	$(CONTAINER) build -t $(CI_REGISTRY_IMAGE) .

container-push:
	$(CONTAINER) push $(CI_REGISTRY_IMAGE)
