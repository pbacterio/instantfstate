# Build python enviroment with only runtime dependencies.
FROM python:3.8-slim as builder
RUN apt-get update && \
    apt-get install -qq build-essential && \
    pip install pipenv
RUN mkdir /app
ADD Pipfile* /app/
WORKDIR /app
ENV PIPENV_VENV_IN_PROJECT=true
RUN pipenv install

# Testing dependencies
FROM builder as tester
RUN pipenv install --dev

# Test
FROM tester
COPY . /app
RUN pipenv run make test

# Final image (no development dependencies/files)
FROM python:3.8-slim
EXPOSE 8080
COPY --from=builder /app /app
COPY instantfstate /app/instantfstate
WORKDIR /app
ENTRYPOINT ["/app/.venv/bin/python", "-m", "instantfstate"]