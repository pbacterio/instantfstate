from aiohttp.web import Response
from aiohttp_jinja2 import render_template, get_env, render_string
from jinja2 import Template

from instantfstate.stateid import StateIDFactory

INDEX_TEMPLATE = None


async def index(request):
    tpl_ctx = {
        'stateid': StateIDFactory(request.app['SECRET']).create(),
        'api_url': request.url.with_path('/api')
    }
    global INDEX_TEMPLATE
    if INDEX_TEMPLATE == None or True:
        INDEX_TEMPLATE = get_env(request.app).get_template('index.html')
    return Response(text=INDEX_TEMPLATE.render(tpl_ctx),
                    content_type='text/html')
