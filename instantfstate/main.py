from pathlib import Path

import aiohttp_jinja2
import jinja2
from aiohttp.web import Application, run_app
from os import getenv, getenvb, environ
from sys import exit

from instantfstate import web, state_api, metrics
from instantfstate.backend import PostgresLock, PostgresState


def main():
    for k in ('SECRET', 'DSN'):
        if k not in environ:
            exit(f'FATAL: Configuration var `{k}` is not present')
    app = Application(middlewares=[metrics.metrics_middleware, ])
    app.on_startup.append(async_config)
    run_app(app)


async def async_config(app):
    # Routing
    app.router.add_routes(state_api.routes)
    app.router.add_get('/', web.index)
    app.router.add_get('/metrics', metrics.metrics_view)
    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(str(Path(__file__).parent / 'templates')))

    app['SECRET'] = getenvb(b'SECRET')
    # DSN looks like: 'host=... dbname=... user=... password=...'
    app['LOCK_BACKEND'] = await PostgresLock.new(getenv('DSN'))
    app['STATE_BACKEND'] = await PostgresState.new(getenv('DSN'))


if __name__ == '__main__':
    main()
