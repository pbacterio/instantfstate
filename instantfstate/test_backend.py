from instantfstate.backend import MemoryLock, PostgresLock, MemoryState, PostgresState


async def test_memory_lock():
    ml = MemoryLock()
    assert await ml.lock('id1', b'data-v1') == True
    assert await ml.lock('id2', b'data-v1') == True
    assert await ml.lock('id1', b'data-v2b') == False
    await ml.unlock('id1')
    assert await ml.lock('id1', b'data-v2') == True


async def test_postgres_lock():
    pgmock = PostgresMock()
    pl = PostgresLock(pgmock)

    pgmock.rows = ()
    assert await pl.lock('id1', b'data-v1') == True

    pgmock.rows = ()
    assert await pl.lock('id2', b'data-v1') == True

    pgmock.rows = ((1,),)
    assert await pl.lock('id1', b'data-v2b') == False

    await pl.unlock('id1')

    pgmock.rows = ()
    assert await pl.lock('id1', b'data-v2') == True


async def test_memory_state():
    ms = MemoryState()
    await ms.set('id1', b'payload1')
    await ms.set('id2', b'payload2')
    assert await ms.get('id1') == b'payload1'
    assert await ms.get('id2') == b'payload2'
    await ms.set('id1', b'payload1a')
    assert await ms.get('id1') == b'payload1a'


async def test_postgres_state():
    pgmock = PostgresMock()
    ps = PostgresState(pgmock)

    pgmock.rows = ()
    await ps.set('id1', b'payload1')

    pgmock.rows = ()
    await ps.set('id2', b'payload2')

    pgmock.rows = ((b'payload1',),)
    assert await ps.get('id1') == b'payload1'

    pgmock.rows = ((b'payload2',),)
    assert await ps.get('id2') == b'payload2'

    pgmock.rows = ((1,),)
    await ps.set('id1', b'payload1a')

    pgmock.rows = ((b'payload1a',),)
    assert await ps.get('id1') == b'payload1a'


class PostgresMock:

    def __init__(self):
        self.queries = []
        self.rows = []

    async def execute(self, query, params):
        self.queries.append((query, params))

    @property
    def rowcount(self):
        return len(self.rows)

    async def fetchone(self):
        return self.rows[0] if self.rows else None

    async def cursor(self):
        return self

    def __enter__(self):
        return self

    def __exit__(self, *args, **kargs):
        pass
