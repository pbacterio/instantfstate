from typing import Dict, Optional

import aiopg  # type: ignore


class MemoryLock:
    locks: Dict[str, bytes] = {}

    async def lock(self, stateid: str, content: bytes) -> bool:
        if stateid in self.locks:
            return False
        self.locks[stateid] = content
        return True

    async def unlock(self, stateid: str):
        if stateid in self.locks:
            del self.locks[stateid]


class MemoryState:
    states: Dict[str, bytes] = {}

    async def get(self, stateid: str) -> Optional[bytes]:
        return self.states.get(stateid)

    async def set(self, stateid: str, content: bytes):
        self.states[stateid] = content


class PostgresBackend:

    def __init__(self, pool: aiopg.Pool):
        self.pool = pool


class PostgresLock(PostgresBackend):

    @staticmethod
    async def new(dsn: str):
        pool = await aiopg.create_pool(dsn=dsn, minsize=0, enable_json=False, enable_hstore=False, enable_uuid=False)
        return PostgresLock(pool)

    async def lock(self, stateid: str, content: bytes) -> bool:
        with (await self.pool.cursor()) as cur:
            await cur.execute('SELECT 1 FROM locks WHERE stateid=%s LIMIT 1', (stateid,))
            if cur.rowcount == 1:
                return False
            await cur.execute('INSERT INTO locks (stateid, content) VALUES (%s, %s)', (stateid, content))
        return True

    async def unlock(self, stateid: str):
        with (await self.pool.cursor()) as cur:
            await cur.execute('DELETE FROM locks WHERE stateid=%s', (stateid,))


class PostgresState(PostgresBackend):

    @staticmethod
    async def new(dsn: str):
        pool = await aiopg.create_pool(dsn=dsn, minsize=0, enable_json=False, enable_hstore=False, enable_uuid=False)
        return PostgresState(pool)

    async def get(self, stateid: str) -> Optional[bytes]:
        with (await self.pool.cursor()) as cur:
            await cur.execute('SELECT content FROM states WHERE stateid=%s LIMIT 1', (stateid,))
            row = await cur.fetchone()
            if row is None:
                return None
            return row[0]

    async def set(self, stateid: str, content: bytes):
        with (await self.pool.cursor()) as cur:
            await cur.execute('SELECT 1 FROM states WHERE stateid=%s LIMIT 1', (stateid,))
            if cur.rowcount == 1:
                # Update
                await cur.execute('UPDATE states SET content=%s, updated_at=now() WHERE stateid=%s', (content, stateid))
            else:
                # Insert
                await cur.execute('INSERT INTO states (stateid, content) VALUES (%s, %s)', (stateid, content))
