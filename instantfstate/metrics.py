from time import time
from typing import Callable, Awaitable

from aiohttp.web import Request, Response, middleware, StreamResponse
from aiohttp.web_exceptions import HTTPException
from prometheus_client import generate_latest, Counter, Histogram  # type: ignore

from .state_api import handler as api_handler
from .web import index as web_handler

REQUEST_COUNT = Counter('http_requests_total', 'HTTP requests', ('code', 'handler'))
LATENCY_HISTOGRAM = Histogram('http_requests_latency_seconds', 'HTTP requests latency (seconds)', ('handler', 'method'))

HANDLER_NAME = {
    web_handler: 'web',
    api_handler: 'api'
}


async def metrics_view(request: Request) -> Response:
    return Response(body=generate_latest(), content_type='text/plain')


@middleware
async def metrics_middleware(request: Request, handler: Callable[[Request], Awaitable[StreamResponse]]):
    if request.path == '/metrics' or handler == metrics_view:
        return await handler(request)
    raise_exception = None
    start_time = time()
    try:
        resp = await handler(request)
        code = resp.status
    except HTTPException as e:
        code = e.status
        raise_exception = e
    finally:
        end_time = time()
    handler_name = HANDLER_NAME.get(handler, '__no_handler__')
    code_simple = f'{code // 100}xx'
    REQUEST_COUNT.labels(code_simple, handler_name).inc()
    LATENCY_HISTOGRAM.labels(handler_name, request.method).observe(end_time - start_time)
    if raise_exception:
        raise raise_exception
    return resp
