from aiohttp.web import Application

from instantfstate.backend import MemoryLock, MemoryState
from instantfstate.state_api import routes
from instantfstate.stateid import StateIDFactory


async def test_handler(aiohttp_client, loop):
    app = Application()
    app.router.add_routes(routes)
    app['SECRET'] = b'zxcv'
    app['LOCK_BACKEND'] = MemoryLock()
    app['STATE_BACKEND'] = MemoryState()

    client = await aiohttp_client(app)

    path = '/api/' + StateIDFactory(app['SECRET']).create()
    resp = await client.request('LOCK', path, data=b'lock1')
    assert resp.status == 200

    resp = await client.request('POST', path, data=b'state1')
    assert resp.status == 200

    resp = await client.request('GET', path)
    assert resp.status == 200
    assert (await resp.read()) == b'state1'

    resp = await client.request('GET', path, headers={'Accept-Encoding': 'gzip'})
    assert resp.status == 200
    assert resp.headers.get('Content-Encoding') == 'gzip'
    assert (await resp.read()) == b'state1'

    resp = await client.request('UNLOCK', path)
    assert resp.status == 200
