from instantfstate.stateid import StateIDFactory


def test_create_validate():
    id_factory = StateIDFactory(b'mykey')
    id = id_factory.create()
    assert id_factory.verify(id)


def test_validate_incorrect_id():
    id_factory = StateIDFactory(b'mykey')
    assert id_factory.verify('bm8gdmFsaWQgaWQ=') == False
    assert id_factory.verify('---') == False
    assert id_factory.verify('') == False


def test_randomness():
    id_factory = StateIDFactory(b'mykey')
    assert id_factory.create() != id_factory.create()


def test_create():
    id_factory = StateIDFactory(b'\x92zJ\xbbM_\xcd\x91\xe2\xd3\x0e<3\xb4\xde[',
                                rand_bytes_func=lambda _: b'\xb1\x97\xc9l\x88\xf8\xac\x9c\x08\x1a?\xd8[\xbe\xe7Lu')
    assert id_factory.create() == 'sZfJbIj4rJwIGj_YW77nTHUW0t5WdUN9iqXuoWM5XMRz'


def test_verify():
    id_factory = StateIDFactory(b'\x92zJ\xbbM_\xcd\x91\xe2\xd3\x0e<3\xb4\xde[')
    assert id_factory.verify('sZfJbIj4rJwIGj_YW77nTHUW0t5WdUN9iqXuoWM5XMRz')
