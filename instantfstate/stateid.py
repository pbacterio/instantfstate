from typing import Callable
from os import urandom
from hmac import digest
from base64 import urlsafe_b64encode, urlsafe_b64decode


class StateIDFactory:
    RAND_LEN = 17
    ID_LEN = 44

    def __init__(self, secret: bytes, rand_bytes_func: Callable = urandom):
        self.secret = secret
        self.rand_bytes = rand_bytes_func

    def create(self) -> str:
        rnd = self.rand_bytes(StateIDFactory.RAND_LEN)
        return urlsafe_b64encode(rnd + digest(self.secret, rnd, 'shake128')).decode('ascii')

    def verify(self, id: str) -> bool:
        try:
            if len(id) != StateIDFactory.ID_LEN:
                return False
            id_bytes = urlsafe_b64decode(id)
        except:
            return False
        rnd = id_bytes[:StateIDFactory.RAND_LEN]
        hash = id_bytes[StateIDFactory.RAND_LEN:]
        return hash == digest(self.secret, rnd, 'shake128')
