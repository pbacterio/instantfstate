from gzip import compress, decompress

from aiohttp.web import RouteTableDef, HTTPMethodNotAllowed, HTTPNotFound, HTTPNotImplemented, Response, Request

from instantfstate.stateid import StateIDFactory

routes = RouteTableDef()


@routes.route('*', '/api/{stateid}')
async def handler(request: Request) -> Response:
    if request.method not in ('GET', 'POST', 'DELETE', 'LOCK', 'UNLOCK'):
        raise HTTPMethodNotAllowed
    stateid = request.match_info['stateid']
    id_fact = StateIDFactory(request.app['SECRET'])
    if not id_fact.verify(stateid):
        raise HTTPNotFound()
    return await ACTIONS[request.method](request)


async def get(request):
    stateid = request.match_info['stateid']
    backend = request.app['STATE_BACKEND']
    state = await backend.get(stateid)
    if state:
        if 'gzip' in request.headers.get('Accept-Encoding', '').lower():
            return Response(body=state, content_type='application/json', headers={'Content-Encoding': 'gzip'})
        else:
            return Response(body=decompress(state), content_type='application/json')
    raise HTTPNotFound


async def post(request):
    stateid = request.match_info['stateid']
    backend = request.app['STATE_BACKEND']
    content = await request.read()
    await backend.set(stateid, compress(content))
    return Response()


async def delete(request) -> Response:
    raise HTTPNotImplemented


async def lock(request):
    stateid = request.match_info['stateid']
    content = await request.read()
    backend = request.app['LOCK_BACKEND']
    if await backend.lock(stateid, content):
        return Response()
    return Response(status=423)


async def unlock(request):
    stateid = request.match_info['stateid']
    backend = request.app['LOCK_BACKEND']
    await backend.unlock(stateid)
    return Response()


ACTIONS = {
    'GET': get,
    'POST': post,
    'DELETE': delete,
    'LOCK': lock,
    'UNLOCK': unlock
}
